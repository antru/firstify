%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Type checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- import vertices_edges_to_ugraph/3 from ugraphs.
:- import member/2, append/3 from lists.

:- import predname/3, listtuple/2 from helpers.
:- import scc/2 from scc.

:- export find_declaration_groups/2.

% diff :: ((A => E1), (A => E2)) => A => E3
% union :: ((A => E1), (A => E2)) => A => E3
% gen_union :: (A => E1) => i => A => E2
% bypassed :: (((A, B) => E1), A => E2) => B => E3
% winnow :: (((A, A) => E1), A => E2) => A => E3
% w :: (((A, A) => E1), A => E2) => i => A => E3

:- op(700,xfx,=>).

%
% Find declaration groups by finding the strongly connected components of the
% predicate dependency graph.
%

find_declaration_groups(Rules, DeclarationGroups) :-
  find_definitions(Rules, DefInternal),
  sort(DefInternal, Definitions),
  find_dependencies(Rules, Definitions, DepInternal),
  sort(DepInternal, Dependencies),
  vertices_edges_to_ugraph(Definitions, Dependencies, Graph),
  scc(Graph, DeclarationGroups).

find_definitions([], []).

find_definitions([Rule|Rs], [Def/Arity|Ds]) :-
  Rule ^=.. [:-,Head,_],
  predname(Head, Def, Arity),
  find_definitions(Rs, Ds).

find_definitions([Rule|Rs], [Def/Arity|Ds]) :-
  compound(Rule),
  not proper_hilog(Rule),
  functor(Rule, Def, Arity), 
  find_definitions(Rs, Ds).
  
find_dependencies([], _, []).

find_dependencies([Rule|Rs], Defs, FinalDeps) :-
  Rule ^=.. [:-,Head,Body], !,
  predname(Head, PredName, Arity),
  listtuple(BodyList, Body),
  find_dependencies_body(PredName/Arity, BodyList, Defs, NewDeps),
  find_dependencies(Rs, Defs, InnerDeps),
  append(NewDeps, InnerDeps, FinalDeps).

find_dependencies([_|Rs], Defs, Deps) :-
  find_dependencies(Rs, Defs, Deps).
  
find_dependencies_body(_, [], _, []).

find_dependencies_body(Def, [Term|Ts], Defs, [NewDep|Deps]) :-
  predname(Term, PredName, Arity),
  member(PredName/Arity, Defs), !,
  NewDep = PredName/Arity-Def,
  find_dependencies_body(Def, Ts, Defs, Deps).
  
find_dependencies_body(Def, [_|Ts], Defs, Deps) :-
  find_dependencies_body(Def, Ts, Defs, Deps).
  
%
% 
%

typeof(V, Vt) :- var(V), var(Vt).

typeof(I, i) :- atomic(I).

typeof(T, Res) :- 
  compound(T), not proper_hilog(T),
  T ^=.. [_|B],
  typeof_rec(B,Bs), listtuple(Bs,TBs),
  var(V), Res = (TBs=>V).
  
typeof(T, Res) :- 
  proper_hilog(T),
  T ^=.. [H|B],
  nonvar(H),
  typeof(H, TH => _),
  typeof_rec(B,Bs), listtuple(Bs,TBs),
  var(V), Res = ((TH => TBs)=>V).
  
typeof(T, Res) :- 
  proper_hilog(T),
  T ^=.. [H|B],
  var(H),
  typeof(H, TH => _),
  typeof_rec(B,Bs), listtuple(Bs,TBs),
  var(V), Res = ((TH => TBs)=>V).

typeof_rec([], []).
typeof_rec([X|Xs], [Y|Ys]) :- typeof(X,Y), typeof_rec(Xs, Ys).

