%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Rule Specialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- import gensym/2 from gensym.
:- import append/3, delete/3, reverse/2 from lists.

:- import predicate_rule/2, known_predicate/1, known_specialization/3, 
          add_known_specialization/3, add_specialized_rule/1 from program.
:- import predname/2, mapget/3, membchk/2, matchterm/3, matchsubterm/3,
          renpredname/3, delete_all/3 from helpers.
:- import firstify_rule/2 from firstify.

:- export specialize_term/3, specialize_rule/3.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
%
% Given a Term returns the an equivalent SpecTerm by applying all necessary 
% specializations. The specializations are materialized in a later stage.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

specialize_term(Term, SubstList, SpecTerm) :-
  generalize(Term, GenTerm),
  spec12(GenTerm, SubstList, GenSpecTerm), !,
  matchterm(GenTerm, Term, VarList),
  matchterm(GenSpecTerm, SpecTerm, VarList), !.
  
spec12(Term, SubstList, SpecTerm) :- spec1(Term, SubstList, SpecTerm), !.
spec12(Term, SubstList, SpecTerm) :- spec2(Term, SubstList, SpecTerm), !.

%
% Replace any atom in Term with a variable.
%

generalize(Term, Term) :-
  atom(Term), !.
  
generalize(Term, NewTerm) :-
  compound(Term),
  Term ^=.. [InnerTerm|Rest],
  generalize(InnerTerm, NewInnerTerm),
  generalize_l(Rest, NewRest),
  NewTerm ^=.. [NewInnerTerm|NewRest].
  
generalize_l([], []).
generalize_l([V|As], [V|Bs]) :- var(V), generalize_l(As, Bs).
generalize_l([T|As], [V|Bs]) :- nonvar(T), var(V), generalize_l(As, Bs).
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Specialize using the existing specializations. We assume generalized terms.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spec1(Term, SubstList, SpecTerm) :-
  SpecialTuple = (Term, SubstList, SpecTerm), 
  known_specialization(KnownTerm, KnownSubstList, KnownSpecTerm), % assume one
  KnownSpecialTuple = (KnownTerm, KnownSubstList, KnownSpecTerm),
  spec1le(SpecialTuple, KnownSpecialTuple), !.

%
% The current term matches to the one that is already specialized
%

spec1eq(CurrentSpecialTuple, KnownSpecialTuple) :-
  CurrentSpecialTuple = (CurrTerm, CurrSubstList, CurrSpecTerm), 
  KnownSpecialTuple = (KnownTerm, KnownSubstList, KnownSpecTerm),
  matchterm(KnownTerm, CurrTerm, VarMap), 
  substitute_vars(KnownSubstList, CurrSubstList, VarMap),
  matchterm(KnownSpecTerm, CurrSpecTerm, VarMap).

%
% The current term is smaller than the one that is already specialized.
% Then remove the outer terms for the specialized terms and check again.
%

spec1le(CurrentSpecialTuple, KnownSpecialTuple) :-
  spec1eq(CurrentSpecialTuple, KnownSpecialTuple), !.
  
spec1le(CurrentSpecialTuple, KnownSpecialTuple) :-
  KnownSpecialTuple = (Term, SubstList, SpecTerm),
  Term ^=.. [InnerTerm|L1],
  SpecTerm ^=.. [InnerSpecTerm|L2],
  not(L1 = []),
  not(L2 = []),
  spec1le(CurrentSpecialTuple, (InnerTerm, SubstList, InnerSpecTerm)).
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Specialize by creating a new specialization. We assume generalized terms.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spec2(Term, SubstList, SpecTerm) :-
  term_variables(Term, ArgVars),
  makespecterm(Term, ArgVars, SubstList, SpecTerm),
  SpecialTuple = (Term, SubstList, SpecTerm),
  predname(Term, Pred),
  forall(
    predicate_rule(Pred, Rule), (
      specialize_rule(Rule, SpecialTuple, SpecRule),
      firstify_rule(SpecRule, FirstifiedSpecRule),
      add_specialized_rule(FirstifiedSpecRule) ) 
  ).
  
%
% Changes a term by dropping the variables that were binded by the specialized
% list, and adding the free variables of the substitutions. All variables that 
% are going to be substituted must be contained in the same argument tuple.
% 

makespecterm(Term, _, _, NewTerm) :- 
  atom(Term), 
  gensym(Term, NewTerm), !.
  
makespecterm(Term, ArgVars, SubstList, NewTerm) :-
  compound(Term), 
  Term ^=.. [InnerTerm|Rest], 
  makespecterm(InnerTerm, ArgVars, SubstList, NewInnerTerm),
  makespecterm_l(Rest, ArgVars, SubstList, NewRest),
  ( NewRest = [] -> 
       NewTerm = NewInnerTerm ; 
       NewTerm ^=.. [NewInnerTerm|NewRest] ).

%
% Helper for the above predicate. Operates over a list of arguments.
% 

makespecterm_l(ArgList, _, [], ArgList).

makespecterm_l(ArgList, ArgVars, [eq(Var, Subst)|L], NewArgList) :-
  atom(Subst),
  membchk(Var, ArgList),
  delete(ArgList, Var, InterArgList),
  makespecterm_l(InterArgList, ArgVars, L, NewArgList), !.

makespecterm_l(ArgList, ArgVars, [eq(Var, Subst)|L], NewArgList) :-
  compound(Subst),
  term_variables(Subst, VarL),
  delete_all(VarL, ArgVars, NewVarL),
  membchk(Var, ArgList),
  delete(ArgList, Var, InterArgList),
  append(InterArgList, NewVarL, AlmostNewArgList),
  makespecterm_l(AlmostNewArgList, ArgVars, L, NewArgList), !.
  
makespecterm_l(ArgList, ArgVars, [_|L], NewArgList) :-
  makespecterm_l(ArgList, ArgVars, L, NewArgList).

%
% Specializes one rule given a specialization tuple.
%

specialize_rule(Rule, SpecialTuple, SpecRule) :-
  SpecialTuple = (Term, SubstList, SpecTerm),
  Rule ^=.. [:-, Head, Body],
  matchsubterm(Term, Head, VarList),
  substitute_vars(SubstList, SpecSubstList, VarList),
  predname(SpecTerm, SpecPredName),
  term_variables(Head, ArgVars),
  makespecterm(Head, ArgVars, SpecSubstList, InterHead),
  renpredname(InterHead, SpecPredName, SpecHead),
  substbody(Body, SpecSubstList, SpecBody),
  SpecRule ^=.. [:-, SpecHead, SpecBody],
  remember_specialization(Head, SpecSubstList, SpecHead).

%
% Rename variables of the specialization list.
%

substitute_vars([], [], _).

substitute_vars([eq(X,T1)|Xs], [eq(Y,T2)|Ys], UL) :-
  mapget(X, UL, Y),
  matchterm(T1, T2, UL),
  substitute_vars(Xs, Ys, UL).

substitute_vars([eq(X,T1)|Xs], [eq(X,T2)|Ys], UL) :-
  not mapget(X, UL, _),
  matchterm(T1, T2, UL),
  substitute_vars(Xs, Ys, UL).
    
%
% Changes the body of a rule by adding an expression eq(Var,Subst) for every tuple
% (Var,Subst) in the substitution list
%

substbody(Body, SubstList, NewBody) :-
  reverse(SubstList, SubstListRev),
  substbody_r(Body, SubstListRev, NewBody).

substbody_r(Body, [], Body).

substbody_r(Body, [eq(Var, Subst)|L], NewBody) :-
  InterBody = ','(eq(Var,Subst),Body),
  substbody_r(InterBody, L, NewBody).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Remember already specialized predicates.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

remember_specialization(Term, _, SpecTerm) :-
  known_specialization(KnownTerm, _, KnownSpecTerm),
  matchterm(Term, KnownTerm, _), 
  matchterm(SpecTerm, KnownSpecTerm, _), !.
  
remember_specialization(Term, SubstList, SpecTerm) :-
  generalize(Term, GenTerm),
  generalize(SpecTerm, GenSpecTerm),
  add_known_specialization(GenTerm, SubstList, GenSpecTerm), !.
