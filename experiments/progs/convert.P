%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Convert programs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- import add_lib_dir/1 from consult.

:- add_lib_dir(('../../../src')). 

:- import firstify/4 from firstify.
:- import set_type/1, clear_types/0 from program.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- set_type( closure(p)(i,i) ).
:- set_type( maplist(p)(i,i) ).

:- set_type( gen_union(p)(i)(i) ).
:- set_type( gen_diff(p)(i)(i) ).
:- set_type( gen_conj(p)(i)(i) ).
:- set_type( union(p,p)(i) ).
:- set_type( diff(p,p)(i) ).
:- set_type( conj(p,p)(i) ).

:- set_type( union5(p)(i) ).
:- set_type( diff5(p)(i) ).
:- set_type( conj5(p)(i) ).
:- set_type( union10(p)(i) ).
:- set_type( diff10(p)(i) ).
:- set_type( conj10(p)(i) ).

:- set_type( gu(i)(i) ).
:- set_type( gd(i)(i) ).
:- set_type( gc(i)(i) ).

:- set_type( part3_union(i) ).
:- set_type( part4_union(i) ).
:- set_type( part5_union(i) ).
:- set_type( part6_union(i) ).
:- set_type( part3_diff(i) ).
:- set_type( part4_diff(i) ).
:- set_type( part5_diff(i) ).
:- set_type( part6_diff(i) ).


:- set_type( p(i,i) ).
:- set_type( plusone(i,i) ).
:- set_type( q(i)(i) ).
:- set_type( p1(i) ).
:- set_type( p2(i) ).
:- set_type( p3(i) ).
:- set_type( p4(i) ).
:- set_type( p5(i) ).
:- set_type( p6(i) ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

closure_goal :- 
  Goal = closure(p)(X,Y),
  OutputPath = 'closure-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, 'programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
maplist_goal :- 
  Goal = maplist(plusone)(X,Y),
  OutputPath = 'maplist-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, 'programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_union_goal :- 
  Goal = gen_union(gu)(I)(Y),
  OutputPath = './gen_union-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_diff_goal :- 
  Goal = gen_diff(gd)(I)(Y),
  OutputPath = './gen_diff-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_conj_goal :- 
  Goal = gen_conj(gc)(I)(Y),
  OutputPath = './gen_conj-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

union5_goal :- 
  Goal = union5(gu)(Y),
  OutputPath = './union5-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
diff5_goal :- 
  Goal = diff5(gd)(Y),
  OutputPath = './diff5-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
conj5_goal :- 
  Goal = conj5(gc)(Y),
  OutputPath = './conj5-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
union10_goal :- 
  Goal = union10(gu)(Y),
  OutputPath = './union10-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
diff10_goal :- 
  Goal = diff10(gd)(Y),
  OutputPath = './diff10-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
conj10_goal :- 
  Goal = conj10(gc)(Y),
  OutputPath = './conj10-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_part3_union_goal :- 
  Goal = part3_union(X),
  OutputPath = './part3_union-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_part4_union_goal :- 
  Goal = part4_union(X),
  OutputPath = './part4_union-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_part5_union_goal :- 
  Goal = part5_union(X),
  OutputPath = './part5_union-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_part6_union_goal :- 
  Goal = part6_union(X),
  OutputPath = './part6_union-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_part3_diff_goal :- 
  Goal = part3_diff(X),
  OutputPath = './part3_diff-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_part4_diff_goal :- 
  Goal = part4_diff(X),
  OutputPath = './part4_diff-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_part5_diff_goal :- 
  Goal = part5_diff(X),
  OutputPath = './part5_diff-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
gen_part6_diff_goal :- 
  Goal = part6_diff(X),
  OutputPath = './part6_diff-spec.P',
  write('Specializing programs.P using goal: '),
  write(Goal),
  nl,
  firstify(Goal, NewGoal, './programs.P', OutputPath),
  write('Specialized Goal: '),
  write(NewGoal),
  nl.
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- time(timed_call( closure_goal,         [max(5000,fail)] )).
:- time(timed_call( maplist_goal,         [max(5000,fail)] )).
:- time(timed_call( gen_union_goal,       [max(5000,fail)] )).
:- time(timed_call( gen_diff_goal,        [max(5000,fail)] )).
:- time(timed_call( gen_conj_goal,        [max(5000,fail)] )).
:- time(timed_call( union5_goal,       [max(5000,fail)] )).
:- time(timed_call( diff5_goal,        [max(5000,fail)] )).
:- time(timed_call( conj5_goal,        [max(5000,fail)] )).
:- time(timed_call( union10_goal,       [max(5000,fail)] )).
:- time(timed_call( diff10_goal,        [max(5000,fail)] )).
:- time(timed_call( conj10_goal,        [max(5000,fail)] )).
:- time(timed_call( gen_part3_union_goal, [max(5000,fail)] )).
:- time(timed_call( gen_part4_union_goal, [max(5000,fail)] )).
:- time(timed_call( gen_part5_union_goal, [max(5000,fail)] )).
:- time(timed_call( gen_part6_union_goal, [max(5000,fail)] )).
:- time(timed_call( gen_part3_diff_goal,  [max(5000,fail)] )).
:- time(timed_call( gen_part4_diff_goal,  [max(5000,fail)] )).
:- time(timed_call( gen_part5_diff_goal,  [max(5000,fail)] )).
:- time(timed_call( gen_part6_diff_goal,  [max(5000,fail)] )).

:- clear_types.
