import random
import sys

# lmdb stats:
# 85620 films, 17156 directors (ratio: 4.99)

def pretty_string(prefix, num, width):
  return prefix + str(num).zfill(width)
  
def make_list(prefix, size):
  width = len(str(size))
  return map(lambda num: pretty_string(prefix,num,width), range(1,size+1))

if len(sys.argv) < 2:
  print "Usage: python generateMovies.py [#movies] [#directors]"
  exit()

movies_num = int(sys.argv[1])
directors_num = int(sys.argv[2])

name = "movie"
directors = make_list("d",directors_num)

runtime_from = 100
runtime_to = 260

rating_from = 15
rating_to = 95

genres = [ 
  "action",
  "adventure", 
  "animation", 
  "biography", 
  "comedy", 
  "crime", 
  "documentary",
  "drama", 
  "family", 
  "fantasy",
  "film_noir",
  "history",
  "horror", 
  "music",
  "musical",
  "mystery", 
  "romance", 
  "science_fiction", 
  "sport", 
  "thriller", 
  "war", 
  "western"
  ]


for x in range(1,movies_num+1):
  res = []
  res.append("m" + str(x))
  res.append("title_of_m" + str(x))
  res.append(random.choice(directors))
  res.append(random.choice(genres))
  res.append(str(random.randrange(runtime_from,runtime_to)))
  res.append(str(random.randrange(rating_from,rating_to)))
  print name + "((" + ",".join(res) + "))." 
