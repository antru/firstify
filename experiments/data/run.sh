python generateData.py 1000 > data_1000.P
python generateData.py 2000 > data_2000.P
python generateData.py 4000 > data_4000.P
python generateData.py 6000 > data_6000.P
python generateData.py 8000 > data_8000.P

python generateBin.py 1000 > bdata_1000.P
python generateBin.py 2000 > bdata_2000.P
python generateBin.py 4000 > bdata_4000.P
python generateBin.py 6000 > bdata_6000.P
python generateBin.py 8000 > bdata_8000.P

python generateGData.py 10 1000 > gdata_1000.P
python generateGData.py 10 2000 > gdata_2000.P
python generateGData.py 10 4000 > gdata_4000.P
python generateGData.py 10 6000 > gdata_6000.P
python generateGData.py 10 8000 > gdata_8000.P

python generateMovies.py    10   10 > movie_10.P
python generateMovies.py    25   10 > movie_25.P
python generateMovies.py    50   10 > movie_50.P
python generateMovies.py    75   10 > movie_75.P
python generateMovies.py   100   20 > movie_100.P
python generateMovies.py   500  100 > movie_500.P
python generateMovies.py  1000  200 > movie_1000.P
python generateMovies.py  2000  400 > movie_2000.P
python generateMovies.py  4000  800 > movie_4000.P
python generateMovies.py  8000 1600 > movie_8000.P
python generateMovies.py 10000 2000 > movie_10000.P

python generateGraph.py  1  5 > graph_1_05.P
python generateGraph.py  1 10 > graph_1_10.P
python generateGraph.py  1 20 > graph_1_20.P
python generateGraph.py  2  5 > graph_2_05.P
python generateGraph.py  2 10 > graph_2_10.P
python generateGraph.py  2 20 > graph_2_20.P
python generateGraph.py  3  5 > graph_3_05.P
python generateGraph.py  3 10 > graph_3_10.P
python generateGraph.py  4  5 > graph_4_05.P
python generateGraph.py  4 10 > graph_4_10.P
python generateGraph.py  5  5 > graph_5_05.P
python generateGraph.py  5 10 > graph_5_10.P
python generateGraph.py 10 10 > graph_10_10.P
python generateGraph.py 10 20 > graph_10_20.P


