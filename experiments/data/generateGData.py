import random
import sys

if len(sys.argv) < 2:
  print "Usage: python generateGData.py [#num] [#size]"
  exit()

data_num = int(sys.argv[1])
data_size = int(sys.argv[2])

name = "gdata"

num_from = 100
num_to   = 99999

for i in range(1,data_num+1):
  for x in range(1,data_size+1):
    res = []
    res.append(str(i))
    res.append(str(x))
    res.append(str(random.randrange(num_from,num_to)))
    res.append(str(random.randrange(num_from,num_to)))
    res.append(str(random.randrange(num_from,num_to)))
    print name + "(" + ",".join(res) + ")." 
