import random
import sys

if len(sys.argv) < 1:
  print "Usage: python generateBin.py [#size]"
  exit()

data_size = int(sys.argv[1])

name = "bin"

num_from = 100
num_to   = 99999

for x in range(1,data_size+1):
  res = []
  res.append(str(x))
  res.append(str(x+1))
  res.append(str(x+2))
  res.append(str(x+3))
  res.append(str(random.randrange(num_from,num_to)))
  res.append(str(random.randrange(num_from,num_to)))
  res.append(str(random.randrange(num_from,num_to)))
  print name + "(" + ",".join(res) + ")." 
