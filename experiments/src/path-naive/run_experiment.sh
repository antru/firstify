export SUITE="path"
export EXPERIMENT="naive"

export DATAFILES="
graph_1_05
graph_1_10
graph_1_20
graph_2_05
graph_2_10
graph_2_20
graph_3_05
graph_3_10
graph_4_05
"

export HILOG_GOAL="naive_shortest(edge,a,z,X)"
export PROLOG_GOAL="naive_shortest5(a,z,X)"

../run_experiment.sh

