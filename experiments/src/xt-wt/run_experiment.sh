export SUITE="xt"

export DATAFILES="
movie_100
movie_500
movie_1000
movie_2000
"

export EXPERIMENT="wt2"
export HILOG_GOAL="wt(c1_pref,movie)(2)(X)"
export PROLOG_GOAL="wt443(2,X)"

../run_experiment.sh

# export DATAFILES="
# movie_100
# movie_500
# movie_1000
# "
# 
# export EXPERIMENT="wt3"
# export HILOG_GOAL="wt(c1_pref,movie)(3)(X)"
# export PROLOG_GOAL="wt443(3,X)"
# 
# ../run_experiment.sh

