export SUITE="diff5"
export EXPERIMENT="exp"

export DATAFILES="
gdata_1000
gdata_2000
gdata_4000
gdata_6000
gdata_8000
"

export HILOG_GOAL="diff5(gd)(X)"
export PROLOG_GOAL="diff520(X)"

../run_experiment.sh
