:- hilog w, winnow, bypassed, gen_union, union, diff, c1_pref, movie.

w(C,R)(1)(T) :- winnow(C,R)(T).
w(C,R)(N)(T) :- N>1,M is N-1, winnow(C,diff(R,gen_union(w(C,R))(M)))(T).

winnow(C,R)(T) :- R(T), not bypassed(C,R)(T).
bypassed(C,R)(T) :- R(Z), C(Z,T).

gen_union(R)(1)(X) :- R(1)(X).
gen_union(R)(N)(X) :- N>1, M is N-1, union(gen_union(R)(M),R(N))(X).

union(R,Q)(X) :- R(X).
union(R,Q)(X) :- Q(X).

diff(P,Q)(X) :- P(X), not Q(X).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c1_pref((I1,N1,D1,G,M1,R1),(I2,N2,D2,G,M2,R2)) :- R1 > R2.
