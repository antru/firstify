export SUITE="gen_union"

export DATAFILES="
gdata_1000
gdata_2000
gdata_4000
gdata_6000
gdata_8000
"

export EXPERIMENT="n-5"
export HILOG_GOAL="gen_union(gu)(5)(X) "
export PROLOG_GOAL="gen_union5(5,X)"

../run_experiment.sh

export EXPERIMENT="n-10"
export HILOG_GOAL="gen_union(gu)(10)(X) "
export PROLOG_GOAL="gen_union5(10,X)"

../run_experiment.sh


