export SUITE="conj10"
export EXPERIMENT="exp"

export DATAFILES="
gdata_1000
gdata_2000
gdata_4000
gdata_6000
gdata_8000
"

export HILOG_GOAL="conj10(gc)(X)"
export PROLOG_GOAL="conj1054(X)"

../run_experiment.sh
