EXPERIMENTS="
closure
conj5
conj10
diff5
diff10
union5
union10
gen_conj
gen_diff
gen_union
winnow
xt-w
xt-wt
path-naive
path-dag
"

for EXPERIMENT in `echo $EXPERIMENTS`
do
  
  echo "Started experiment "$EXPERIMENT"..."
  cd $EXPERIMENT
  ./run_experiment.sh
  cd ..
  
done
