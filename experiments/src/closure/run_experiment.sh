export SUITE="closure"
export EXPERIMENT="exp"

export DATAFILES="
bdata_1000
bdata_2000
bdata_4000
bdata_6000
bdata_8000
"

export HILOG_GOAL="closure(p)(X,Y)"
export PROLOG_GOAL="closure256(X,Y)"

../run_experiment.sh
