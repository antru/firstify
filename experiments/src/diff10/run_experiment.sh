export SUITE="diff10"
export EXPERIMENT="exp"

export DATAFILES="
gdata_1000
gdata_2000
gdata_4000
gdata_6000
gdata_8000
"

export HILOG_GOAL="diff10(gd)(X)"
export PROLOG_GOAL="diff1043(X)"

../run_experiment.sh
