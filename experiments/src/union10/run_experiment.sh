export SUITE="union10"
export EXPERIMENT="exp"

export DATAFILES="
gdata_1000
gdata_2000
gdata_4000
gdata_6000
gdata_8000
"

export HILOG_GOAL="union10(gu)(X)"
export PROLOG_GOAL="union1032(X)"

../run_experiment.sh
