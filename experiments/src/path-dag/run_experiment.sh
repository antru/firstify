export SUITE="path"
export EXPERIMENT="dag"

export DATAFILES="
graph_1_05
graph_1_10
graph_1_20
graph_2_05
graph_2_10
graph_2_20
graph_3_05
graph_3_10
graph_4_05
"

export HILOG_GOAL="dag_shortest(edge,a,z,X)"
export PROLOG_GOAL="dag_shortest10(a,z,X)"

../run_experiment.sh

