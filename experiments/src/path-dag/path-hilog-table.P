:- hilog dag_shortest, dag_path_pref, dag_path, winnow, bypassed, edge.
:- table apply/2, apply/3, apply/4, apply/5.

dag_shortest(G,X,Y,C):-winnow(dag_path_pref(G),dag_path(G))((X,Y,C)).

dag_path_pref(G)((X,Y,C1),(X,Y,C2)) :- 
  dag_path(G)((X,Y,C1)),
  dag_path(G)((X,Y,C2)),
  C1 < C2.

dag_path(G)((X,Y,C)) :- G((X,Y,C)).
dag_path(G)((X,Y,C)) :- G((X,Z,C1)),
  winnow(dag_path_pref(G),dag_path(G))((Z,Y,C2)),C is C1+C2.

winnow(C,R)(T) :- R(T), not bypassed(C,R)(T).
bypassed(C,R)(T) :- C(Z,T),R(Z).