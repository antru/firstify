XSB_PATH="/home/antru/bin/XSB/bin/xsb"

# Run experiment

rm *.xwam
rm ../data/*.xwam

# Store experiment

echo "suite,experiment,edb,mode,engine,cputime,elapsetime,results" > results.csv

for DATAFILE in `echo $DATAFILES`
do

  #######################################################################################
  # vanilla hilog in xsb
  
  LOGFILE="$SUITE-hilog-$DATAFILE.tmp"
  
  echo "['"$SUITE$"-hilog.P']. 
        ['../../data/"$DATAFILE".P']. 
        time(("$HILOG_GOAL", write('result='), write(X), nl, fail))." \
    | $XSB_PATH --quietload --noprompt \
    |& tee $LOGFILE
    
  RESULTS=`grep 'result=' $LOGFILE | wc -l`
  TIMES=`grep ^% $LOGFILE | grep CPU | awk '{ print $2","$5 }'`
  
  echo $SUITE","$EXPERIMENT","$DATAFILE",hilog,xsb,"$TIMES","$RESULTS >> results.csv
  
  
  #######################################################################################
  # tabled hilog in xsb
  
  LOGFILE="$SUITE-hilog-table-$DATAFILE.tmp"
  
  echo "['"$SUITE$"-hilog-table.P']. 
        ['../../data/"$DATAFILE".P']. 
        time(("$HILOG_GOAL", write('result='), write(X), nl, fail))." \
    | $XSB_PATH --quietload --noprompt \
    |& tee $LOGFILE
    
  RESULTS=`grep 'result=' $LOGFILE | wc -l`
  TIMES=`grep ^% $LOGFILE | grep CPU | awk '{ print $2","$5 }'`
  
  echo $SUITE","$EXPERIMENT","$DATAFILE",hilog-table,xsb,"$TIMES","$RESULTS >> results.csv
    
  rm ../../data/"$DATAFILE".xwam
  
  
  #######################################################################################
  # vanilla prolog in xsb
  
  LOGFILE="$SUITE-xsb-prolog-$DATAFILE.tmp"
  
  echo "['"$SUITE$"-prolog.P']. 
        ['../../data/"$DATAFILE".P']. 
        time(("$PROLOG_GOAL", write('result='), write(X), nl, fail))." \
    | $XSB_PATH --quietload --noprompt \
    |& tee $LOGFILE
    
  RESULTS=`grep 'result=' $LOGFILE | wc -l`
  TIMES=`grep ^% $LOGFILE | grep CPU | awk '{ print $2","$5 }'`
  
  echo $SUITE","$EXPERIMENT","$DATAFILE",prolog,xsb,"$TIMES","$RESULTS >> results.csv
  
    
  #######################################################################################
  # tabled prolog in xsb
    
  LOGFILE="$SUITE-xsb-prolog-table-$DATAFILE.tmp"
  
  echo "['"$SUITE$"-prolog-table.P']. 
        ['../../data/"$DATAFILE".P']. 
        time(("$PROLOG_GOAL", write('result='), write(X), nl, fail))." \
    | $XSB_PATH --quietload --noprompt \
    |& tee $LOGFILE
  
  RESULTS=`grep 'result=' $LOGFILE | wc -l`
  TIMES=`grep ^% $LOGFILE | grep CPU | awk '{ print $2","$5 }'`
  
  echo $SUITE","$EXPERIMENT","$DATAFILE",prolog-table,xsb,"$TIMES","$RESULTS >> results.csv
  
  rm ../../data/"$DATAFILE".xwam
  
  
  #######################################################################################
  # vanilla prolog in swi-prolog
  
  LOGFILE="$SUITE-swi-prolog-$DATAFILE.tmp"
  
  echo "use_module(library(statistics)).
        ['"$SUITE$"-prolog.P']. 
        ['../../data/"$DATAFILE".P']. 
        time(("$PROLOG_GOAL", write('result='), write(X), nl, fail))." \
    | swipl \
    |& tee $LOGFILE
    
  RESULTS=`grep 'result=' $LOGFILE | wc -l`
  TIMES=`grep ^% $LOGFILE | grep CPU | awk '{ print $4","$7 }'`
  
  echo $SUITE","$EXPERIMENT","$DATAFILE",prolog,swi,"$TIMES","$RESULTS >> results.csv
  
    
  #######################################################################################
  # vanilla prolog in yap
  
  LOGFILE="$SUITE-yap-prolog-$DATAFILE.tmp"
  
  echo "['"$SUITE$"-prolog.P']. 
        ['../../data/"$DATAFILE".P']. 
        time(("$PROLOG_GOAL", write('result='), write(X), nl, fail))." \
    | yap \
    |& tee $LOGFILE
    
  RESULTS=`grep 'result=' $LOGFILE | wc -l`
  TIMES=`grep CPU $LOGFILE | awk '{ print $2","$5 }'`
  
  echo $SUITE","$EXPERIMENT","$DATAFILE",prolog,yap,"$TIMES","$RESULTS >> results.csv
  
done

DIR="../../runs/"$SUITE.$EXPERIMENT.`date +"%Y-%m-%d.%T"`
mkdir $DIR
grep ^% *.tmp | grep CPU > stats.txt
mv stats.txt results.csv $DIR

rm *.tmp
rm *.xwam

