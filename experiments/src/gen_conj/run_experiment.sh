export SUITE="gen_conj"

export DATAFILES="
gdata_1000
gdata_2000
gdata_4000
gdata_6000
gdata_8000
"

export EXPERIMENT="n-5"
export HILOG_GOAL="gen_conj(gc)(5)(X) "
export PROLOG_GOAL="gen_conj11(5,X)"

../run_experiment.sh

export EXPERIMENT="n-10"
export HILOG_GOAL="gen_conj(gc)(10)(X) "
export PROLOG_GOAL="gen_conj11(10,X)"

../run_experiment.sh


