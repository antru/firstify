export SUITE="winnow"
export EXPERIMENT="c1"

export DATAFILES="
movie_1000
movie_2000
movie_4000
movie_8000
movie_10000
"

export HILOG_GOAL="winnow(c1_pref,movie)(X)"
export PROLOG_GOAL="winnow467(X)"

../run_experiment.sh

